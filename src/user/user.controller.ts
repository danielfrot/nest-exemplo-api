import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { PrismaService } from '../prisma.service'
import { User as UserModel, Post as PostModel, Prisma } from '@prisma/client'

@Controller('v1/user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  async newUser(@Body() userData: UserModel) {
    await this.userService.newUser(userData);
  }

  @Get()
  async getAllUsers(): Promise<UserModel[]> {
    return this.userService.getAllUsers();
  }

  @Get(':id')
  async getUserById(@Param('id') id: string): Promise<UserModel> {
    return this.userService.getUserById(id);
  }
}
