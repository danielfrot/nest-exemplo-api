import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { User as UserModel, Post as PostModel, Prisma } from '@prisma/client';

@Injectable()
export class UserService {
  constructor(private prismaService: PrismaService) {}

  async getAllUsers(): Promise<UserModel[]> {
    return this.prismaService.user.findMany();
  }

  async newUser(userData: UserModel | any): Promise<void> {
    const { name, email, posts } = userData;
    console.log(posts);
    await this.prismaService.user.create({
      data: {
        name,
        email,
        posts: {
          create: {
            title: 'testeeee',
          },
        },
      },
    });
  }

  async getUserById(param_id: string): Promise<UserModel> {
    console.log(typeof param_id);
    const id_int = parseInt(param_id);
    return this.prismaService.user.findUnique({
      where: {
        id: id_int,
      },
    });
  }

  // async user(userWhereUniqueInput: Prisma.UserWhereUniqueInput): Promise<UserModel | null> {
  //   return this.prisma.user.findUnique({
  //     where: userWhereUniqueInput,
  //   });
  // }

  // async users(params: {
  //   skip?: number;
  //   take?: number;
  //   cursor?: Prisma.UserWhereUniqueInput;
  //   where?: Prisma.UserWhereInput;
  //   orderBy?: Prisma.UserOrderByInput;
  // }): Promise<UserModel[]> {
  //   const { skip, take, cursor, where, orderBy } = params;
  //   return this.prisma.user.findMany({
  //     skip,
  //     take,
  //     cursor,
  //     where,
  //     orderBy,
  //   });
  // }

  // async createUser(data: Prisma.UserCreateInput): Promise<UserModel> {
  //   return this.prisma.user.create({
  //     data,
  //   });
  // }

  // async updateUser(params: {
  //   where: Prisma.UserWhereUniqueInput;
  //   data: Prisma.UserUpdateInput;
  // }): Promise<UserModel> {
  //   const { where, data } = params;
  //   return this.prisma.user.update({
  //     data,
  //     where,
  //   });
  // }

  // async deleteUser(where: Prisma.UserWhereUniqueInput): Promise<UserModel> {
  //   return this.prisma.user.delete({
  //     where,
  //   });
  // }
}